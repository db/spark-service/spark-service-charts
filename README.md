# Helm Chart for Spark Service at CERN

Helm charts for:
- ***spark services***(CVMFS, S3, Spark History, Spark Shuffle)
- ***spark user account***(limited to single namespace for user isolation)

Note this requires helm version 3.x

##### Quickstart

Install spark services and user

```bash
# Install the spark services chart
# this also installs a pod cleaner CronJob that cleans Completed and Failed pod for Spark executors 
helm install --namespace spark --set cvmfs.enable=true --create-namespace spark cern-spark-services-1.0.5.tgz

# Example of how to install a spark user chart, one chart per user (SWAN takes care of this for new users)
export USER=...
helm install spark-user-${USER} cern-spark-user-1.0.5.tgz --namespace ${USER} --create-namespace --set cvmfs.enable=true --set namespace=${USER}
```

Note, the charts have also been pushed to registry.cern.ch, to install from there:
```
helm install --namespace spark --set cvmfs.enable=true --create-namespace spark oci://registry.cern.ch/swan/charts/sparkk8s/cern-spark-services --version 1.0.5

export USER=....
helm install spark-user-${USER} oci://registry.cern.ch/swan/charts/sparkk8s/cern-spark-user --version 1.0.5 --namespace ${USER} --create-namespace --set cvmfs.enable=true --set namespace=${USER}
```

To display information on how to access Kubernetes Dashboard, Grafana Monitoring and History Server check:

```bash
$ helm status -n spark spark 
$ helm status -n ${USER} spark-user-${USER}
```

### Packaging charts

```
$ ./helm package ./cern-spark-services
$ ./helm package ./cern-spark-user
```

### Spark Charts Guide

##### Installing Services

The chart can be installed by running:

```
$ helm install --namespace spark --set cvmfs.enable=true --create-namespace spark cern-spark-services-1.0.5.tgz
$ helm install --namespace spark --set cvmfs.enable=true --create-namespace spark cern-spark-services-1.0.5.tgz
 
# Check if Spark Services are running
$ helm status spark
```

Chart can be deleted using

```
$ helm del -n spark spark
```

The chart can be upgraded by running:

```
$ helm upgrade --set [new-key=new-value] spark [path-to-new-tgz]
$ helm upgrade --recreate-pods --set [new-key=new-value] spark [path-to-new-tgz]
```

The chart can be customized by extending values from [cern-spark-services/values.yaml](cern-spark-services/values.yaml):

```
$ cat <<EOF >>services-values.yaml
... other values ...
cvmfs:
  enable: true
  repository:
    - sft.cern.ch
EOF
 
$ helm install -f services-values.yaml --name spark ./cern-spark-services-1.0.0.tgz
```

##### Installing Spark Users

User spark is initialized for particular isolated namespace (queue)

```
USER=default
helm install spark-user-${USER} cern-spark-user-1.0.5.tgz --namespace ${USER} --create-namespace --set cvmfs.enable=true --set namespace=${USER}
```

Generate config following instruction from helm

```bash
$ helm status -n ${NAMESPACE} spark-user-${NAMESPACE}
```
